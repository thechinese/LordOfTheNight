# Thurael MALFURION

# Persönlichkeit:

# Ausrüstung: 

# TODO?

# Logbuch: neue Einträge oben

## Intermezzo Reise:
- Reise durch den Wald, 
- Lagerfeuer, umgegrabenes Stück Erde -> Dirk ignoriert beides, schaut sich aber um -> Trupp zur bekämpfung
- Klippe gefunden, wo ein stück fehlt
- Dorf mit gerohdeten Flächen, Verscheucht Dorfbewohner mit Drohung -> Vermuten dass er zu Chaos gehört?

- 3 Leute mit spitzhacke
- X in Mine	
- Leute in die Mine gelaufen
- Wache verwundet am Boden
- Kutscher mit Pfeil in der Brust 
- Mage#1 mit 2xSpiegelbild + Kugelblitz cast, Pfeil in linkes Bein
- Mage#2 in Hütte -> Ritual wenn zu lange, ansonsten Flucht

- Hütte #1 angezündet + Karren vor Mineneingang, noch ein Pfeil in eine Person

- Loot: 
	- Pergamente: Hilfesuche an Lords, 
	- Logbücher mit [Calth](../Places/Calth.md), Macragge, Veridan
	
## Logbuch: Dirk
- Tag 1: 
	- [Traum](../Places/Traum.md)
	- Auftag von Druidenzirkel erhalten -> Aufbruch reise
- Tag 2: Reise durch Wald
	- umgegrabenes Gebiet, Lagerfeuer einer Truppe entdeckt, mehr Spuren führen zum Graben als weg
	- Entdeckung von kleinem Dorf (Name), Dirk verschreckt Holzfäller und Verbrennt + Vergräbt Äxte da sie Bäume gefällt haben.
- Tag 3: 
	- Mine entdeckt
- Tag 4: 
	- [Calth](../Places/Calth.md) entdeckt und aus der Ferne beobachtet
	- 
	
## Prolog Dirk: [Traum](../Places/Traum.md) 
- Druidenzirkel, Auftrag in seinen Waldpart zu gehen
	-> beschütze den Wald dort
	- Hindernisse: seltsame Spuren
