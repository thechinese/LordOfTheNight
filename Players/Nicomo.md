# Nicomo Cosca

# Welt
- Königreich Ultramar
- 

# Persönlichkeit:
- Arm aufgewachsen -> Goldgierig
- Skrupellos
- Goldgierig
- Alkoholiker
- Es muss noch mehr geben im Leben?

# Aspekte
- Vom Straßenkind zum Degenmeister
- Berüchigter Glücksritter
- Alkoholiker aus Leidenschaft

# Fertigkeiten
- +4: Degen
- +3: (Wurf-)messer, Überlebenskünstler
- +2: ehemaliger Söldnergeneral, Überzeugen, dreckige Tricks
- +1: Gassenwissen, Zechen, Taschendiebstahl, Geldwesen

# Attribute
- +3: Beweglichkeit
- +2: Willenskraft, Konstitution
- +1: Charisma, Intelligenz, Stärke

# Skills:
- Degenkämpfer, Duellist 
- Dreckige Tricks: versteckte Messer
- Schlachterfahrung, oft aber nur indirekt

# Ausrüstung:
- Waffen: 
- Rapier von Konrad -> schlichter Stahldegen
    - 5 (Wurf-)Messer (3 am Gürtel & jeweils 1 an jeder Wade)
- Kleidung: 
    - Lederrüstung ohne Helm, darüber eine rot-gelber Umhang 
    - feste braune Lederstiefel mit jeweils 1 Goldmünze in jeder Sohle versteckt

# Logbuch: neue Einträge oben

- Im Moment incognito als Käsehändler unterwegs in Richtung Calth, um die Kontaktperson des Händlers zu treffen
- Mein Händlerwagen gehörte ursprünglich einem richtigen Käsehändler names Tancred 
- Da ich jedoch der Garde in Nostramo half, ihn aus dem Weg zu räumen, wurde mir sein Wagen vermacht
- Verbindungen zur Garde in Nostramo wurden geknüpft. Ich könnte mich weiterhin als nützlich für sie erweisen. Mein Kontakt: Konrad

- Treffen mit Konrad und Barnabas
- Auftrag von Konrad, in einer Mine ein Buch zu suchen
  ->Buch von Logar - Grimmoir -> hat das Potenzial, die Welt zu verändern
  - Minengänger: Magnus, 5 Arbeiter, 2 Aufseher
   - Magnus war ein Megiekundiger und hat wohl aus dem Buch gelesen, 
      ist jedoch nicht in anprechbarem Zustand und wird bald hingerichtet (im Momen im Gefängnis)
  - Belohnung ist Gold + Degen (Degen schon als Vorschuss -> solider Rapier)

- Code von Konrads Geheimbund: The armour of contempt is your shield
- Adresse von Konrad: Zum Rum 38, Calth
- 
- Straßenkind beim Verfolgen gestellt: beobachtet Konrads Haus auf Verlangdes des "Herren"
   ->Adresse von "dem Herr": Rabenweg 8 (Der Herr ist Teil der Wache=

