# Regelwerk
Allgemeines Regelwerk

## Charaktere:
- Jeder Charakter lässt sich als Baum darstellen. Jeder Knoten ist ein Aspekt oder ein Skill
- Jeder hat 3-5 Aspekte, welche die Persönlichkeit des Chars gut zusammenfassen würden. 
- Aus denen lassen sich Skills ableiten, je weiter weg vom "Hauptknoten", desto spezialisierter und ungewöhnlicher ist im Normalfall der Skill
- Es ist im Grunde alles möglich als Akspekt zu wählen.

## Attribute:
- Stärke
- Beweglichkeit
- Intelligenz
- Willenskraft
- Konstitution
- Charisma

## Proben:
- Normal wird einfach der passende Skill abgefragt, wenn keine besondere Situation vorliegt -> Kein Roll notwendig
- Die Fertigkeitenwerte bewegen sich normalerweise zwischen -2 bis +8
- Es wird kein W20 gewürfelt, sondern 4x W6-Würfel: 1-2=+ | 3-4=0 | 5-6=-
* zB: Skill Handeln auf 5, Probe wird erschwert weil Char hässlich ist: Rolle W6: 1:+, 2-3:nix, 4-6:-
