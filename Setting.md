# Die Spielwelt

# Magie:
- Allgemein bekannt, aber nicht zwingend weit verbreitet
- Noch nicht für die Allgemeinheit tauglich 
- Sehr vielfältige Manifestierung, viele entwickelen ihre eigenen Stile
- Oft nicht direkt für Kampf geeignet


# Götter:
- Viele Götter, die "Stärke" der Götter hängt von der Macht der jeweiligen Anhänger ab. 
    - Ein Gott des Weines wird zB durch florierende Weinberge gestärkt und 
- Götter greifen fast nie direkt in das Weltgeschehen ein, es gibt häufiger indirekte "Geschenke" an ihre Anhänger
- 


# Ultramar: Anfangspunkt der Geschichte
- Angelehnt an das Mittelalterliche Feudalsystem
- Hauptstadt: Macragge, Sitz des Herrschers Calgar
- In mehrere Bereiche aufgeteilt, jeder Teil hat Gouverneur, der Calgar untersteht
    - Veridia: Östlicher Teil von Ultramar, Hauptsächlich Agrarwirtschaft
        - [Calth](./Places/Calth.md): Schnell wachsendes Dorf
    - Macragge: Hauptstadt
    - Talsa:

