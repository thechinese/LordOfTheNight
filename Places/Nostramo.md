# Prolog [Nicomo](./Players/Nicomo.md): Nostramo

# NPCs:
- Unbenannter Käsehändler (Cheese Dude): A man of virtue is a man of many cheeses
- Anführer der Stadtwache 

# Orte:
- Markt
- Taverne + eigene Hütten für Wohlhabende 

# Events: 
- Aufenthalt in Taverne, Autrag von Konrad erhalten 
- Cheese Dude unterhaltung mit Nicoma auf dem Markt
- Verfolgung und Infiltration von Nicoma zur Hütte vom Cheese Dude
- Lauert dem Käsehändler in seiner Hütte auf, Folter + Raub
- Wird vom Anführer der Stadtwache "rekrutiert" und mit neuer Idendität nach [Calth](/Places/Calth) geschickt


# Notizen
- 